import React, { Component } from 'react'

export default (props) => {

	return (
		<div id="page-wrapper">

			<div id="header-wrapper">
				<div id="header" className="container">

						<h1 id="logo"><a href="index.html">Strongly Typed</a></h1>
						<p>A responsive HTML5 site template. Manufactured by HTML5 UP.</p>

						<nav id="nav">
							<ul>
								<li><a className="icon fa-home" href="index.html"><span>Introduction</span></a></li>
								<li>
									<a href="#" className="icon fa-bar-chart-o"><span>Dropdown</span></a>
									<ul>
										<li><a href="#">Lorem ipsum dolor</a></li>
										<li><a href="#">Magna phasellus</a></li>
										<li><a href="#">Etiam dolore nisl</a></li>
										<li>
											<a href="#">Phasellus consequat</a>
											<ul>
												<li><a href="#">Magna phasellus</a></li>
												<li><a href="#">Etiam dolore nisl</a></li>
												<li><a href="#">Phasellus consequat</a></li>
											</ul>
										</li>
										<li><a href="#">Veroeros feugiat</a></li>
									</ul>
								</li>
								<li><a className="icon fa-cog" href="left-sidebar.html"><span>Left Sidebar</span></a></li>
								<li><a className="icon fa-retweet" href="right-sidebar.html"><span>Right Sidebar</span></a></li>
								<li><a className="icon fa-sitemap" href="no-sidebar.html"><span>No Sidebar</span></a></li>
							</ul>
						</nav>

				</div>
			</div>

			<div id="features-wrapper">
				<section id="features" className="container">
					<header>
						<h2>Gentlemen, behold! This is <strong>Strongly Typed</strong>!</h2>
					</header>
					<div className="row">
						<div className="4u 12u(mobile)">

								<section>
									<a href="#" className="image featured"><img src="/images/pic01.jpg" alt="" /></a>
									<header>
										<h3>Okay, so what is this?</h3>
									</header>
									<p>This is <strong>Strongly Typed</strong>, a free, fully responsive site template
									by <a href="http://html5up.net">HTML5 UP</a>. Free for personal and commercial use under the
									<a href="http://html5up.net/license">CCA 3.0 license</a>.</p>
								</section>

						</div>
						<div className="4u 12u(mobile)">

								<section>
									<a href="#" className="image featured"><img src="/images/pic02.jpg" alt="" /></a>
									<header>
										<h3>Nice! What is HTML5 UP?</h3>
									</header>
									<p><a href="http://html5up.net">HTML5 UP</a> is a side project of <a href="http://twitter.com/ajlkn">AJ’s</a> (= me).
									I started it as a way to both test my <strong>skel</strong> framework and sharpen up my coding
									and design skills a bit.</p>
								</section>

						</div>
						<div className="4u 12u(mobile)">

								<section>
									<a href="#" className="image featured"><img src="/images/pic03.jpg" alt="" /></a>
									<header>
										<h3>Skel? Whats that?</h3>
									</header>
									<p><strong>Skel</strong> is a lightweight framework for building responsive
									sites and apps. All of my stuff at <a href="http://html5up.net">HTML5 UP</a> (including this
									one) are built on this framework.</p>
								</section>

						</div>
					</div>
					<ul className="actions">
						<li><a href="#" className="button icon fa-file">Tell Me More</a></li>
					</ul>
				</section>
			</div>

			<div id="banner-wrapper">
				<div className="inner">
					<section id="banner" className="container">
						<p>Use this space for <strong>profound thoughts</strong>.<br />
						Or an enormous ad. Whatever.</p>
					</section>
				</div>
			</div>

			<div id="main-wrapper">
				<div id="main" className="container">
					<div className="row">

							<div id="content" className="8u 12u(mobile)">

									<article className="box post">
										<header>
											<h2><a href="#">I don’t want to say <strong>it’s the aliens</strong> ...<br />
											but it’s the aliens.</a></h2>
										</header>
										<a href="#" className="image featured"><img src="/images/pic04.jpg" alt="" /></a>
										<h3>I mean isnt it possible?</h3>
										<p>Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit
										ligula vel quam viverra sit amet mollis tortor congue. Sed quis mauris
										sit amet magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada
										in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat
										magna tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros
										consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id
										justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet
										mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique.
										Curabitur leo nibh, rutrum eu malesuada in, tristique at erat.</p>
										<ul className="actions">
											<li><a href="#" className="button icon fa-file">Continue Reading</a></li>
										</ul>
									</article>

									<article className="box post">
										<header>
											<h2><a href="#">By the way, many thanks to <strong>regularjane</strong>
											for these awesome demo photos</a></h2>
										</header>
										<a href="#" className="image featured"><img src="/images/pic05.jpg" alt="" /></a>
										<h3>You should probably check out her work</h3>
										<p>Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit
										ligula vel quam viverra sit amet mollis tortor congue. Sed quis mauris
										sit amet magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada
										in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat
										consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id
										in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat
										magna tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros
										consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id
										justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet
										mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique.
										Curabitur leo nibh, rutrum eu malesuada in, tristique at erat.</p>
										<p>Erat lorem ipsum veroeros consequat magna tempus lorem ipsum consequat
										Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit ligula
										vel quam viverra sit amet mollis tortor congue. Sed quis mauris sit amet
										magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada in,
										tristique at erat. Curabitur leo nibh, rutrum eu malesuada  in, tristique
										at erat lorem ipsum dolor sit amet lorem ipsum sed consequat magna
										tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros consequat
										magna tempus</p>
										<ul className="actions">
											<li><a href="#" className="button icon fa-file">Continue Reading</a></li>
										</ul>
									</article>

							</div>

							<div id="sidebar" className="4u 12u(mobile)">

									<section>
										<ul className="divided">
											<li>

													<article className="box excerpt">
														<header>
															<span className="date">July 30</span>
															<h3><a href="#">Just another post</a></h3>
														</header>
														<p>Lorem ipsum dolor odio facilisis convallis. Etiam non nunc vel est
														suscipit convallis non id orci lorem ipsum sed magna consequat feugiat lorem dolore.</p>
													</article>

											</li>
											<li>

													<article className="box excerpt">
														<header>
															<span className="date">July 28</span>
															<h3><a href="#">And another post</a></h3>
														</header>
														<p>Lorem ipsum dolor odio facilisis convallis. Etiam non nunc vel est
														suscipit convallis non id orci lorem ipsum sed magna consequat feugiat lorem dolore.</p>
													</article>

											</li>
											<li>

													<article className="box excerpt">
														<header>
															<span className="date">July 24</span>
															<h3><a href="#">One more post</a></h3>
														</header>
														<p>Lorem ipsum dolor odio facilisis convallis. Etiam non nunc vel est
														suscipit convallis non id orci lorem ipsum sed magna consequat feugiat lorem dolore.</p>
													</article>

											</li>
										</ul>
									</section>

									<section>
										<ul className="divided">
											<li>

													<article className="box highlight">
														<header>
															<h3><a href="#">Something of note</a></h3>
														</header>
														<a href="#" className="image left"><img src="/images/pic06.jpg" alt="" /></a>
														<p>Phasellus  sed laoreet massa id justo mattis pharetra. Fusce suscipit ligula vel quam
														viverra sit amet mollis tortor congue magna lorem ipsum dolor et quisque ut odio facilisis
														convallis. Etiam non nunc vel est suscipit convallis non id orci. Ut interdum tempus
														facilisis convallis. Etiam non nunc vel est suscipit convallis non id orci.</p>
														<ul className="actions">
															<li><a href="#" className="button icon fa-file">Learn More</a></li>
														</ul>
													</article>

											</li>
											<li>

													<article className="box highlight">
														<header>
															<h3><a href="#">Something of less note</a></h3>
														</header>
														<a href="#" className="image left"><img src="/images/pic07.jpg" alt="" /></a>
														<p>Phasellus  sed laoreet massa id justo mattis pharetra. Fusce suscipit ligula vel quam
														viverra sit amet mollis tortor congue magna lorem ipsum dolor et quisque ut odio facilisis
														convallis. Etiam non nunc vel est suscipit convallis non id orci. Ut interdum tempus
														facilisis convallis. Etiam non nunc vel est suscipit convallis non id orci.</p>
														<ul className="actions">
															<li><a href="#" className="button icon fa-file">Learn More</a></li>
														</ul>
													</article>

											</li>
										</ul>
									</section>

							</div>

					</div>
				</div>
			</div>

			<div id="footer-wrapper">
				<div id="footer" className="container">
					<header>
						<h2>Questions or comments? <strong>Get in touch:</strong></h2>
					</header>
					<div className="row">
						<div className="6u 12u(mobile)">
							<section>
								<form method="post" action="#">
									<div className="row 50%">
										<div className="6u 12u(mobile)">
											<input name="name" placeholder="Name" type="text" />
										</div>
										<div className="6u 12u(mobile)">
											<input name="email" placeholder="Email" type="text" />
										</div>
									</div>
									<div className="row 50%">
										<div className="12u">
											<textarea name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div className="row 50%">
										<div className="12u">
											<a href="#" className="form-button-submit button icon fa-envelope">Send Message</a>
										</div>
									</div>
								</form>
							</section>
						</div>
						<div className="6u 12u(mobile)">
							<section>
								<p>Erat lorem ipsum veroeros consequat magna tempus lorem ipsum consequat Phaselamet
								mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique. Curabitur
								leo nibh, rutrum eu malesuada.</p>
								<div className="row">
									<div className="6u 12u(mobile)">
										<ul className="icons">
											<li className="icon fa-home">
												1234 Somewhere Road<br />
												Nashville, TN 00000<br />
												USA
											</li>
											<li className="icon fa-phone">
												(000) 000-0000
											</li>
											<li className="icon fa-envelope">
												<a href="#">info@untitled.tld</a>
											</li>
										</ul>
									</div>
									<div className="6u 12u(mobile)">
										<ul className="icons">
											<li className="icon fa-twitter">
												<a href="#">@untitled-tld</a>
											</li>
											<li className="icon fa-instagram">
												<a href="#">instagram.com/untitled-tld</a>
											</li>
											<li className="icon fa-dribbble">
												<a href="#">dribbble.com/untitled-tld</a>
											</li>
											<li className="icon fa-facebook">
												<a href="#">facebook.com/untitled-tld</a>
											</li>
										</ul>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div id="copyright" className="container">
					<ul className="links">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</div>
			</div>

		</div>
	)

}